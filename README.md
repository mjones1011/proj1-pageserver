Author: Mason Jones
Contact: masonj@uoregon.edu
Purpose: 

To gain experience with GIT workflow with separate configuration: Fork the project, make and test changes locally, commit; turn in configuration file with reference to repo.

To extend a tiny web server in Python, to check understanding of basic web architecture

To use automated tests to check progress (plus manual tests for good measure)